package com.demo.myapplication

import android.os.Bundle
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.usizy.sizebutton.UsizyButton
import com.usizy.sizebutton.listeners.UsizyButtonListenerAdapter
import com.usizy.sizebutton.model.UsizyButtonConfiguration
import com.usizy.sizequeryandroid.model.SaveRecommendResponse

class MainActivity : AppCompatActivity(), UsizyButtonListenerAdapter {

    private val productIds = arrayOf(
        "BABY_UPPER",
        "BOY_FULL",
        "BOY_LOWER",
        "BOY_UPPER",
        "CHILD_FULL",
        "CHILD_UPPER",
        "FEMALE_FULL",
        "FEMALE_LOWER",
        "FEMALE_UPPER",
        "GIRL_FULL",
        "GIRL_LOWER",
        "GIRL_UPPER",
        "HUMAN",
        "MALE_FULL",
        "MALE_LOWER",
        "MALE_UPPER",
        "PANT_WOMAN",
        "UNISEX_UPPER",
        "UNISEX",
        "UNISEX_FULL",
        "UNISEX_LOWER",
        "HUMAN_UPPER"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        val btnBaby = findViewById<UsizyButton>(R.id.btnUsizy)
        val config = UsizyButtonConfiguration()
        config.productId = "BABY_UPPER"
        config.user = "AnonymousUser"
        config.logoResId = R.drawable.mango
        btnBaby.initialize(config)
        findViewById<TextView>(R.id.title).text = config.productId
        btnBaby.setOnSizeListener(this)

        val myListAdapter = MyListAdapter(this, productIds)
        findViewById<ListView>(R.id.listView).adapter = myListAdapter
    }

    override fun onRecommendedSize(productId: String?, response: SaveRecommendResponse?) {
        Toast.makeText(this, " $productId: ${response?.usizySize}", Toast.LENGTH_SHORT).show()
    }
}